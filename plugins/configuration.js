export default ({ app }) => {
  app.config = require('../configuration.json')

  // The Environment Variable API_URL takes precedence over the configuration.json
  app.config.api.baseUrl = (app.context.env.baseUrl || app.config.api.baseUrl)
}
