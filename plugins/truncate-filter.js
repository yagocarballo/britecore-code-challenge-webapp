import Vue from 'vue'

Vue.filter('truncate', (text, stop, clamp) => {
  if (text && text.length > 0) {
    return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
  }
  return ''
})
