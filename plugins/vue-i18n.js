import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export default ({ app, store, route, redirect }) => {
  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch
  app.i18n = new VueI18n({
    locale: store.state.locale,
    fallbackLocale: 'en',
    messages: {
      'en': require('../locales/en.json'),
      'es': require('../locales/es.json')
    }
  })

  app.i18n.path = (link) => {
    return `/${app.i18n.locale}/${link}`
  }

  app.i18n.translate = (lang) => {
    return route.fullPath.replace(`/${app.i18n.locale}/`, `/${lang}/`)
  }
}
