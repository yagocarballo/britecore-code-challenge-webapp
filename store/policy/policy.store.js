import axios from 'axios'

import { POLICY_MUTATIONS } from './policy.mutations'

export default {
  state: {
    list: new Array(0),
    creating: {
      policySaved: false,
      sendingPolicy: false,
      lastPolicy: null,
      response: null,
      error: null
    }
  },
  getters: {
    allPolicies: state => {
      return state.list
    },
    policyBeingCreated: state => {
      return state.creating.sendingPolicy
    },
    lastPolicy: state => {
      if (state.creating.lastPolicy) {
        return state.creating.lastPolicy.risk_type
      }
      return ''
    },
    policySaved: state => {
      return state.creating.policySaved
    },
    createPolicyResponse: state => {
      return state.creating.response
    },
    createPolicyError: state => {
      switch (state.creating.error) {
        case 'IntegrityError':
          return 'form.newPolicy.IntegrityError'
        default:
          return state.creating.error
      }
    }
  },
  mutations: {
    [POLICY_MUTATIONS.ADD] (state, policy) {
      state.list.push({
        ...policy
      })
    },
    [POLICY_MUTATIONS.REMOVE] (state, policy) {
      state.list = state.list.filter((policy) => {
        return policy.type !== policy
      })
    },
    [POLICY_MUTATIONS.SET] (state, policies) {
      state.list = policies
    },
    [POLICY_MUTATIONS.CREATE] (state, newPolicy) {
      state.creating = {
        response: null,
        policySaved: false,
        sendingPolicy: true,
        lastPolicy: newPolicy
      }
    },
    [POLICY_MUTATIONS.CREATE_COMPLETE] (state, success, response) {
      state.creating = {
        ...state.creating,
        error: null,
        response: response,
        policySaved: success,
        sendingPolicy: false
      }
    },
    [POLICY_MUTATIONS.CREATE_ERROR] (state, error) {
      state.creating = {
        ...state.creating,
        error: error,
        response: null,
        policySaved: false,
        sendingPolicy: false
      }
    }
  },
  actions: {
    async getPolicies ({app, state, commit}) {
      try {
        let {data} = await axios.get(this.app.config.api.baseUrl + 'policy')
        commit(POLICY_MUTATIONS.SET, data.objects)
      } catch (error) {
        console.log(error.response.data)
      }
    },
    async removePolicy ({app, state, commit}, policy) {
      try {
        await axios.delete(this.app.config.api.baseUrl + 'policy/' + policy)
        commit(POLICY_MUTATIONS.REMOVE, policy)
      } catch (error) {
        console.log(error.response.data)
      }
    },
    async createPolicy ({state, commit}, newPolicy) {
      commit(POLICY_MUTATIONS.CREATE, newPolicy)
      try {
        let response = await axios.post(this.app.config.api.baseUrl + 'policy', newPolicy)
        let success = response.status === 201
        commit(POLICY_MUTATIONS.CREATE_COMPLETE, success, response.response)
        if (success) {
          commit(POLICY_MUTATIONS.ADD, newPolicy)
        }
      } catch (error) {
        commit(POLICY_MUTATIONS.CREATE_ERROR, error.response.data.message)
      }
    }
  }
}
