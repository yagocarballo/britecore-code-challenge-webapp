import Vuex from 'vuex'
import RiskStoreModule from './risk/risk.store'
import PolicyStoreModule from './policy/policy.store'

const createStore = () => {
  return new Vuex.Store({
    state: {
      locales: ['en', 'es'],
      locale: 'en'
    },
    mutations: {
      setLang (state, locale) {
        if (state.locales.indexOf(locale) !== -1) {
          state.locale = locale
        }
      }
    },
    modules: {
      risk: RiskStoreModule,
      policy: PolicyStoreModule
    }
  })
}

export default createStore
