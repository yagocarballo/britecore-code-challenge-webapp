import axios from 'axios'

import { RISK_MUTATIONS } from './risk.mutations'

export default {
  state: {
    list: new Array(0),
    creating: {
      riskSaved: false,
      sendingRisk: false,
      lastRisk: null,
      response: null,
      error: null
    }
  },
  getters: {
    allRisks: state => {
      return state.list
    },
    riskBeingCreated: state => {
      return state.creating.sendingRisk
    },
    lastRiskType: state => {
      if (state.creating.lastRisk) {
        return state.creating.lastRisk.type
      }
      return ''
    },
    riskTypeSaved: state => {
      return state.creating.riskSaved
    },
    createRiskResponse: state => {
      return state.creating.response
    },
    createRiskError: state => {
      switch (state.creating.error) {
        case 'IntegrityError':
          return 'form.newRisk.IntegrityError'
        default:
          return state.creating.error
      }
    }
  },
  mutations: {
    [RISK_MUTATIONS.ADD] (state, risk) {
      state.list.push({
        ...risk
      })
    },
    [RISK_MUTATIONS.REMOVE] (state, riskType) {
      state.list = state.list.filter((risk) => {
        return risk.type !== riskType
      })
    },
    [RISK_MUTATIONS.SET] (state, riskTypes) {
      state.list = riskTypes
    },
    [RISK_MUTATIONS.CREATE] (state, newRiskType) {
      state.creating = {
        response: null,
        riskSaved: false,
        sendingRisk: true,
        lastRisk: newRiskType
      }
    },
    [RISK_MUTATIONS.CREATE_COMPLETE] (state, success, response) {
      state.creating = {
        ...state.creating,
        error: null,
        response: response,
        riskSaved: success,
        sendingRisk: false
      }
    },
    [RISK_MUTATIONS.CREATE_ERROR] (state, error) {
      state.creating = {
        ...state.creating,
        error: error,
        response: null,
        riskSaved: false,
        sendingRisk: false
      }
    }
  },
  actions: {
    async getRiskTypes ({app, state, commit}) {
      try {
        let {data} = await axios.get(this.app.config.api.baseUrl + 'risk')
        commit(RISK_MUTATIONS.SET, data.objects)
      } catch (error) {
        console.log(error.response.data)
      }
    },
    async removeRiskType ({app, state, commit}, riskType) {
      try {
        await axios.delete(this.app.config.api.baseUrl + 'risk/' + riskType)
        commit(RISK_MUTATIONS.REMOVE, riskType)
      } catch (error) {
        console.log(error.response.data)
      }
    },
    async createRiskType ({state, commit}, newRiskType) {
      commit(RISK_MUTATIONS.CREATE, newRiskType)
      try {
        let response = await axios.post(this.app.config.api.baseUrl + 'risk', newRiskType)
        let success = response.status === 201
        commit(RISK_MUTATIONS.CREATE_COMPLETE, success, response.response)
        if (success) {
          commit(RISK_MUTATIONS.ADD, newRiskType)
        }
      } catch (error) {
        commit(RISK_MUTATIONS.CREATE_ERROR, error.response.data.message)
      }
    }
  }
}
