# britecore-challenge

> Product Development Hiring Project

## Build Setup

``` bash
# install dependencies
$ yarn install # or `npm install`

# serve with hot reload at localhost:3000
$ yarn dev # or `npm run dev`

# build for production
$ yarn build # or `npm run build`

# launch the server (this step, builds before starting the server)
$ yarn start # or `npm start`

# Generate Static Page to be used without the Nuxt Server
$ yarn generate # or `npm run generate`
```

## Third Party Libraries Used

- [Nuxt.js](https://github.com/nuxt/nuxt.js) - Used for a quick starting structure of VueJS (as it adds Vue, Vuex, Vuelidate, ... and it has a clean structure)
- [Vue.js](http://vuejs.org) - This is the Web Framework I Used
- [Vuex](https://vuex.vuejs.org/en/) - This is for state management (Redux style)
- [Vue Material](https://vuematerial.io) - Used for Quick Bootstrapping of the GUI.
- [Axios](https://github.com/axios/axios) - Used for Async HTTP Requests (Mostly for speed of development, and to keep code simple to read, otherwise I would had used the native Request)
- [Vuelidate](https://monterail.github.io/vuelidate/) - Used for form Validation.
- [Vue-i18n](https://github.com/Haixing-Hu/vue-i18n) - Used for multi language support

## Structure

I followed the nuxt folder structure. This are the folders and their contents:

| Folder | Description |
| ------ | ----------- |
| assets | External files to be used by the application (that don't need pre-processing) | 
| components | Vue Components used in the application. |
| layouts | Page structure of the application (Sidebar, Header, ...) |
| locales | Translation JSONs used for multi-language support |
| middleware | Code that evaluates before loading a Vue Page (used for injecting the Multi-Language library) |
| pages | Pages available in the application. (they are all under the _lang folder as this folder will be replaced with the language) |
| plugins | Definitions of libraries used by VueJS (Like Vue Material or Vue-i18n (Multi Language)) |
| static | Folder with static content | 
| store | Stores with the state management of the application | 

> 
> The file `configuration.json` is used for setting up the API URL. This file is loaded into the Vue Application as a Plugin.
> 


## Design Decisions

I made some design decisions on how to structure the page and some others for development speed.

### Components

I split some components into separate files (template, style and code), when I the components are too big (as it is easier to manage) and kept them all in one .vue file when they are small.

### Stores

Stores are on their own folder, so for example the store with the state to manage the risk types is in the `store/risk` folder. (for policy it would be `store/policy`, even though I did not do the policy one in this example)

### Forms

I kept the forms as components (I usually like to do this as then forms can be re-used in other places (although, in this example they are just used in one place))

### GUI

I tried to keep the GUI simple, so it is not very fancy.
