module.exports = {
  env: {
    baseUrl: process.env.API_URL || null
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'britecore-challenge',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Product Development Hiring Project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,700,400italic|Material+Icons' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#448aff' },
  /*
  ** Build configuration
  */
  css: [
    { src: 'vue-material/dist/vue-material.min.css', lang: 'css' },
    { src: '~/assets/theme.scss', lang: 'scss' } // include vue-material theme engine
  ],
  router: {
    middleware: 'i18n'
  },
  plugins: [
    { src: '~/plugins/configuration' },
    { src: '~/plugins/vue-material' },
    { src: '~/plugins/vue-i18n' },
    { src: '~plugins/truncate-filter.js' }
  ],
  build: {
    /*
    ** Run ESLint on save
    */
    vendor: ['vue-material', 'vue-i18n'],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    vendor: ['vue-material', 'vue-i18n'],
    routes: [
      '/',
      '/es',
      '/en',
      '/en/new-policy',
      '/en/new-risk-type',
      '/en/view-all-risk-types',
      '/es/new-policy',
      '/es/new-risk-type',
      '/es/view-all-risk-types'
    ]
  }
}
